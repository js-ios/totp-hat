import React, {
  Alert,
  Text,
  TouchableOpacity,
  View
} from 'react-native'
import Clipboard from 'react-native-clipboard'

import styles from '../assets/styles'
import generateOTP from '../lib/totp'

const CodeGeneratorItem  = React.createClass({
  getInitialState() {
    return {
      copiedAt: null,
      currentTime: this.getCurrentTime(),
      manageMode: false,
    }
  },

  componentDidMount() {
    this.intervalTimer = setInterval(() => {
      this.setState({ currentTime: this.getCurrentTime() })
    }, 1000)
  },

  componentWillUnmount() {
    clearInterval(this.intervalTimer)
  },

  getCurrentTime() {
    return Date.now() / 1000
  },

  copy(code) {
    return () => {
      Clipboard.set(code)
      this.setState({ copiedAt: this.state.currentTime })
    }
  },

  remove() {
    this.props.removeCodeGenerator(this.props.generator)
  },

  toggleManageMode() {
    this.setState({ manageMode: !this.state.manageMode })
  },

  renderCopiedNotice() {
    // Show message for 3 seconds
    if (this.state.copiedAt && this.state.currentTime - this.state.copiedAt < 3) {
      return (
        <Text style={ [styles.textDefault, styles.copiedNotice] }>Copied</Text>
      )
    }
  },

  render() {
    const generator = this.props.generator
    const {otp, secondsBeforeExpiration} = generateOTP(generator)

    if (this.state.manageMode) {
      return (
        <View style={ [styles.codeGeneratorListItem, styles.options ] }>
          <TouchableOpacity onPress={ this.remove } style={ styles.option }>
            <Text style={ styles.textDefault }>Delete</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={ this.toggleManageMode } style={ styles.option }>
            <Text style={ styles.textDefault }>Cancel</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <TouchableOpacity onPress={ this.copy(otp) } onLongPress={ this.toggleManageMode }>
          <View style={ styles.codeGeneratorListItem }>
            { this.renderCopiedNotice() }
            <View style={ styles.otpContainer }>
              <Text style={ [styles.textDefault, styles.otp] }>{ otp }</Text>
            </View>
            <View style={ styles.otpMetaContainer }>
              <View style={ styles.otpMeta }>
                <Text style={ styles.textDefault }>{ generator.name }</Text>
              </View>
              <View style={ styles.otpMeta }>
                <Text style={ styles.textDefault }>{ secondsBeforeExpiration }</Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
      )
    }
  }
})

export default CodeGeneratorItem
