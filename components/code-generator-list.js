import React, {
  AsyncStorage,
  ScrollView,
  Text,
  View
} from 'react-native'

import styles from '../assets/styles'
import NewCodeGeneratorForm from './new-code-generator-form'
import CodeGeneratorItem from './code-generator-item'

const asyncStorageKey = 'TotpHat:data:generators'

const CodeGeneratorList  = React.createClass({
  getInitialState() {
    return {
      generators: []
    }
  },

  componentWillMount() {
    this.setRightButtonAction()
    this.fetchGenerators()
  },

  setRightButtonAction() {
    var route = this.props.navigator.navigationContext.currentRoute;
    route.onRightButtonPress = () => {
      this.props.navigator.push({
        title: 'Add a Code',
        component: NewCodeGeneratorForm,
        passProps:  {
          addCodeGenerator: this.addCodeGenerator
        },
        leftButtonTitle: 'Back',
        onLeftButtonPress: () => {
          this.props.navigator.pop()
        }
      })
    }

    this.props.navigator.replace(route)
  },

  fetchGenerators() {
    AsyncStorage.getItem(asyncStorageKey, (error, result) => {
      if (error) return Alert.alert('Uh oh.', "We couldn't retrieve your code generators.")
      if (!result) return
      this.setState({ generators: JSON.parse(result) })
    })
  },

  addCodeGenerator(newGenerator) {
    const generators = this.state.generators.slice(0)
    generators.push(newGenerator)
    this.saveGenerators(generators, "We couldn't save your new code generator.")
    this.props.navigator.pop()
  },

  removeCodeGenerator(generator) {
    const oldGenerators = this.state.generators.slice(0)
    const generators = oldGenerators.filter((oldGenerator) => {
      return !(oldGenerator.name === generator.name && oldGenerator.secret === generator.secret)
    })
    this.saveGenerators(generators, "We couldn't remove that code generator.")
  },

  saveGenerators(generators, errorMessage) {
    AsyncStorage.setItem(asyncStorageKey, JSON.stringify(generators), (error) => {
      if (error) return Alert.alert('Uh oh.', errorMessage)
      this.fetchGenerators()
    })
  },

  renderGenerator(generator) {
    return (
      <CodeGeneratorItem
        key={ generator.name + generator.secret }
        generator={ generator }
        removeCodeGenerator={ this.removeCodeGenerator } />
    )
  },

  renderGeneratorList() {
    if (this.state.generators.length < 1) return (<Text style={ styles.emptyStateMessage }>You don't have any codes yet.</Text>)

    return (
      <View style={ styles.codeGeneratorList }>
        { this.state.generators.map(this.renderGenerator) }
      </View>
    )
  },

  render() {
    return (
      <View style={ styles.page }>
        <ScrollView>
          { this.renderGeneratorList() }
        </ScrollView>
      </View>
    )
  }
})

export default CodeGeneratorList
