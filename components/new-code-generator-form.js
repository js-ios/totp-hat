import React, {
  Dimensions,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import Camera from 'react-native-camera'
import URL from 'url-parse'
import qs from 'qs'

import styles, {colors} from '../assets/styles'

const NewCodeGeneratorForm  = React.createClass({
  getInitialState() {
    return {
      errors: [],
      newGeneratorName: null,
      newGeneratorSecret: null,
      savingGenerator: false,
      showScanner: true,
    }
  },

  validateNewGenerator() {
    const errors = []
    this.setState({ errors })

    if (!this.state.newGeneratorName) {
      errors.push("Please provide an account name.")
    }

    if (!this.state.newGeneratorSecret) {
      errors.push("Please provide a secret key.")
    } else if (this.state.newGeneratorSecret.length !== 16) {
      errors.push(`Please provide a 16 character-long secret key. Length was ${this.state.newGeneratorSecret.length}.`)
    }

    const noErrors = errors.length === 0

    if (!noErrors) this.setState({ errors })

    return noErrors
  },

  addCodeGenerator() {
    const valid = this.validateNewGenerator()

    if (valid) {
      this.props.addCodeGenerator({ name: this.state.newGeneratorName, secret: this.state.newGeneratorSecret })
      this.setState({ newGeneratorName: null, newGeneratorSecret: null })
    }
  },

  barCodeRead(e) {
    if (this.state.savingGenerator) return

    const url = new URL(e.data)

    if (url.protocol === 'otpauth:' && url.host === 'totp') {
      const name = decodeURIComponent(url.pathname).replace(/^\//, '')
      const secret = qs.parse(url.query.replace(/^\?/, '')).secret

      this.setState({ savingGenerator: true })
      this.props.addCodeGenerator({ name, secret })
    }
  },

  renderErrors() {
    if (this.state.errors.length < 1) return null

    return this.state.errors.map((error) => {
      return (
        <Text key={ error } style={ styles.textError }>{ error }</Text>
      )
    })
  },

  renderScanner() {
    const { height, width } = Dimensions.get('window')

    return (
      <View>
        <Text style={ [styles.textDefault, styles.textCentered] }>Add via QR code</Text>
        <Camera
          onBarCodeRead={ this.barCodeRead }
          style={ [styles.cameraPreview, { height: height - 120, width: width - 20 } ]}
          aspect={ Camera.constants.Aspect.Fill }>
          <Text style={ styles.manualEntry } onPress={ () => this.setState({ showScanner: false })}>enter info manually</Text>
        </Camera>
      </View>
    )
  },

  renderForm() {
    return (
      <View style={ styles.newCodeGeneratorForm }>
        <Text style={ styles.textDefault }>Add a new code generator</Text>
        { this.renderErrors() }
        <TextInput
          style={ styles.input }
          value={ this.state.newGeneratorName }
          onChangeText={ (text) => this.setState({ newGeneratorName: text }) }
          placeholder="Account Name"
          placeholderTextColor={ colors.mediumGrey } />
        <TextInput style={ styles.input }
          value={ this.state.newGeneratorSecret }
          onChangeText={ (text) => this.setState({ newGeneratorSecret: text }) }
          placeholder="Secret Key"
          placeholderTextColor={ colors.mediumGrey }
          autoCapitalize="none"
          autoCorrect={ false } />
        <TouchableOpacity onPress={ this.addCodeGenerator }>
          <View style={ styles.button }>
            <Text style={ styles.textButton }>Save</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  },

  renderInputMethod() {
    return this.state.showScanner ? this.renderScanner() : this.renderForm()
  },

  render() {
    return (
      <View style={ [styles.page, styles.pageOrange] }>
        <ScrollView>
          { this.renderInputMethod() }
        </ScrollView>
      </View>
    )
  }
})

export default NewCodeGeneratorForm
