/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
import React, {
  AppRegistry,
  NavigatorIOS,
  StatusBarIOS
} from 'react-native'

import CodeGeneratorList from './components/code-generator-list'
import styles, {colors} from './assets/styles'

const TotpHat  = React.createClass({
  componentWillMount() {
    StatusBarIOS.setStyle('light-content', false)
  },

  render() {
    // State updates don't propogate through Navigator. :-(
    return (
      <NavigatorIOS
        style={ styles.nav }
        initialRoute={{
          title: 'TotpHat',
          component: CodeGeneratorList,
          rightButtonTitle: '+',
        }}
        barTintColor={ colors.darkGrey }
        titleTextColor={ colors.white }
        translucent={ false } />
    )
  }
})

AppRegistry.registerComponent('TotpHat', () => TotpHat)
