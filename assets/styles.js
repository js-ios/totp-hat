import { StyleSheet } from 'react-native'

export const colors = {
  blue: '#007aff',
  darkGrey: '#282828',
  mediumGrey: '#555',
  orange: '#f16c20',
  white: '#eee',
}

export default StyleSheet.create({
  // General Styles
  nav: {
    flex: 1
  },
  page: {
    backgroundColor: colors.darkGrey,
    borderTopColor: colors.blue,
    borderTopWidth: 1,
    flex: 1,
    padding: 10,
  },
  pageOrange: {
    borderTopColor: colors.orange,
  },
  container: {
    flex: 1,
    backgroundColor: colors.darkGrey,
    paddingTop: 25
  },
  textDefault: {
    backgroundColor: 'transparent',
    color: colors.white,
    fontSize: 18,
  },
  textCentered: {
    textAlign: 'center'
  },
  textRightAligned: {
    textAlign: 'right'
  },
  textError: {
    color: colors.orange,
  },
  emptyStateMessage: {
    borderColor: colors.mediumGrey,
    borderWidth: 0.5,
    color: colors.mediumGrey,
    padding: 10,
    textAlign: 'center',
  },
  options: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
  option: {
    borderColor: colors.mediumGrey,
    borderWidth: 1,
    margin: 5,
    padding: 20,
  },

  // Form Styles
  input: {
    borderColor: colors.mediumGrey,
    borderWidth: 1,
    color: colors.white,
    height: 40,
    marginTop: 5,
    padding: 10,
  },
  button: {
    borderColor: colors.mediumGrey,
    borderWidth: 1,
    height: 40,
    justifyContent: 'center',
    marginTop: 5,
    padding: 10,
  },
  textButton: {
    color: colors.mediumGrey,
    fontSize: 18,
  },

  // Generator List Styles
  codeGeneratorList: {
  },
  codeGeneratorListItem: {
    alignItems: 'center',
    backgroundColor: colors.darkGrey,
    borderColor: colors.mediumGrey,
    borderWidth: 1,
    flex: 1,
    margin: 10,
    marginBottom: 0,
    padding: 25,
  },
  otpMetaContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  otpMeta: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  otp: {
    fontFamily: 'Menlo',
    fontSize: 75
  },
  copiedNotice: {
    color: colors.blue,
    fontSize: 12,
  },

  // New Generator Styles
  newCodeGeneratorForm: {
    margin: 10,
    paddingTop: 10,
  },
  cameraPreview: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'flex-end',
  },
  manualEntry: {
    backgroundColor: colors.white,
    color: colors.darkGrey,
    flex: 0,
    margin: 40,
    padding: 10,
  }
})
